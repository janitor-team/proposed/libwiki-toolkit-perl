%Wiki::Toolkit::TestConfig::config = (
                                       'search_invertedindex' => 0,
                                       'plucene' => 0,
                                       'Pg' => {
                                                 'dbname' => undef
                                               },
                                       'dbixfts' => undef,
                                       'MySQL' => {
                                                    'dbname' => undef
                                                  },
                                       'SQLite' => {
                                                     'dbname' => 't/sqlite-test.db'
                                                   }
                                     );
$Wiki::Toolkit::TestConfig::configured = 1;
1;
